package ordination;

import java.time.LocalDate;
import java.util.LinkedList;

public class PN extends Ordination {

    private double antalEnheder;
    private LinkedList<LocalDate> dosisGivetDatoer = new LinkedList<>();

    public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antal) {
        super(startDen, slutDen, laegemiddel);
        this.antalEnheder = antal;
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen
     * Returnerer true hvis givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     *
     * @param givesDen
     * @return
     */
    public boolean givDosis(LocalDate givesDen) {
        if (!givesDen.isBefore(super.getStartDen()) && !givesDen.isAfter(super.getSlutDen())) {
            dosisGivetDatoer.add(givesDen);
            return true;
        } else
            return false;
    }

    public double doegnDosis() {
        return samletDosis() / super.antalDage();
    }

    @Override
    public String getType() {
        return "PN";
    }

    public double samletDosis() {
        return antalEnheder * getAntalGangeGivet();
    }

    /**
     * Returnerer antal gange ordinationen er anvendt
     *
     * @return
     */
    public int getAntalGangeGivet() {
        return dosisGivetDatoer.size();
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

}
