package ordination;

import java.time.*;

public class DagligFast extends Ordination {

	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double morgenAntal,
			double middagAntal, double aftenAntal, double natAntal) {
		super(startDen, slutDen, laegemiddel);
		doser[0] = new Dosis(null, morgenAntal);
		doser[1] = new Dosis(null, middagAntal);
		doser[2] = new Dosis(null, aftenAntal);
		doser[3] = new Dosis(null, natAntal);
	}

	@Override
	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double sum = 0;
		for (Dosis d : doser) {
			sum += d.getAntal();
		}
		return sum;
	}

	@Override
	public String getType() {
		return "Daglig Fast";
	}

	public Dosis[] getDoser() {
		return doser;
	}
}
