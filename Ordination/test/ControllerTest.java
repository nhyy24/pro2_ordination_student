package test;

import controller.Controller;
import ordination.*;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ControllerTest {

    private Controller controller;

    private Patient patient;
    private Laegemiddel laegemiddel;
    private LocalDate startDen;

    private LocalTime[] klokkeSlet;
    private double[] antal;

    @Before
    public void setUp() {
        controller = Controller.getTestController();
        patient = controller.opretPatient("121256-0512", "Jane Jensen", 63.4);
        laegemiddel = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        startDen = LocalDate.of(2020, 2, 20);
        klokkeSlet = new LocalTime[]{LocalTime.of(0, 0), LocalTime.of(12, 0), LocalTime.of(14, 59), LocalTime.of(17, 30), LocalTime.of(22, 2)};
        antal = new double[]{0, 0, 0, 0, 0};
    }

    // testOpretPNOrdination

    @Test
    public void testOpretPNOrdination1() {
        //TC1
        LocalDate slutDen = LocalDate.of(2020, 2, 20);
        double antal = 0.0;
        PN pn = controller.opretPNOrdination(startDen, slutDen, patient, laegemiddel, antal);
        assertNotNull(pn);
    }

    @Test
    public void testOpretPNOrdination2() {
        //TC2
        LocalDate slutDen = LocalDate.of(2020, 2, 29);
        double antal = 5.0;
        PN pn = controller.opretPNOrdination(startDen, slutDen, patient, laegemiddel, antal);
        assertNotNull(pn);
    }

    @Test
    public void testOpretPNOrdination3() {
        //TC3
        LocalDate slutDen = LocalDate.of(2020, 3, 20);
        double antal = 20.0;
        PN pn = controller.opretPNOrdination(startDen, slutDen, patient, laegemiddel, antal);
        assertNotNull(pn);
    }

    @Test
    public void testOpretPNOrdination4() {
        //TC4
        LocalDate slutDen = LocalDate.of(2021, 2, 20);
        double antal = Double.MAX_VALUE;
        PN pn = controller.opretPNOrdination(startDen, slutDen, patient, laegemiddel, antal);
        assertNotNull(pn);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretPNOrdination5() {
        //TC5
        LocalDate slutDen = LocalDate.of(2020, 2, 19);
        double antal = 1.0;
        PN pn = controller.opretPNOrdination(startDen, slutDen, patient, laegemiddel, antal);
        assertNotNull(pn);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretPNOrdination6() {
        //TC6
        LocalDate slutDen = LocalDate.of(2020, 2, 20);
        double antal = -1.0;
        PN pn = controller.opretPNOrdination(startDen, slutDen, patient, laegemiddel, antal);
        assertNotNull(pn);
    }

    // testOpretDagligFastOrdination

    @Test
    public void testOpretDagligFastOrdination1() {
        //TC1
        LocalDate slutDen = LocalDate.of(2020, 2, 20);
        double morgenAntal = 0;
        double middagAntal = 1;
        double aftenAntal = 5;
        double natAntal = Double.MAX_VALUE;
        DagligFast dagligFast = controller.opretDagligFastOrdination(startDen, slutDen, patient, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
        assertNotNull(dagligFast);
    }

    @Test
    public void testOpretDagligFastOrdination2() {
        //TC2
        LocalDate slutDen = LocalDate.of(2020, 2, 29);
        double morgenAntal = 0;
        double middagAntal = 0;
        double aftenAntal = 0;
        double natAntal = 0;
        DagligFast dagligFast = controller.opretDagligFastOrdination(startDen, slutDen, patient, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
        assertNotNull(dagligFast);
    }

    @Test
    public void testOpretDagligFastOrdination3() {
        //TC3
        LocalDate slutDen = LocalDate.of(2020, 3, 20);
        double morgenAntal = 0;
        double middagAntal = 0;
        double aftenAntal = 0;
        double natAntal = 0;
        DagligFast dagligFast = controller.opretDagligFastOrdination(startDen, slutDen, patient, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
        assertNotNull(dagligFast);
    }

    @Test
    public void testOpretDagligFastOrdination4() {
        //TC4
        LocalDate slutDen = LocalDate.of(2021, 2, 20);
        double morgenAntal = 0;
        double middagAntal = 0;
        double aftenAntal = 0;
        double natAntal = 0;
        DagligFast dagligFast = controller.opretDagligFastOrdination(startDen, slutDen, patient, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
        assertNotNull(dagligFast);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligFastOrdination5() {
        //TC5
        LocalDate slutDen = LocalDate.of(2020, 2, 19);
        double morgenAntal = 0;
        double middagAntal = 0;
        double aftenAntal = 0;
        double natAntal = 0;
        DagligFast dagligFast = controller.opretDagligFastOrdination(startDen, slutDen, patient, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
        assertNotNull(dagligFast);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligFastOrdination6() {
        //TC6
        LocalDate slutDen = LocalDate.of(2020, 2, 20);
        double morgenAntal = -1;
        double middagAntal = 0;
        double aftenAntal = 0;
        double natAntal = 0;
        DagligFast dagligFast = controller.opretDagligFastOrdination(startDen, slutDen, patient, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
        assertNotNull(dagligFast);
    }

    // testOpretDagligSkaevOrdination

    @Test
    public void testOpretDagligSkaevOrdination1() {
        //TC1
        LocalDate slutDen = LocalDate.of(2020, 2, 20);
        DagligSkaev dagligSkaev = controller.opretDagligSkaevOrdination(startDen, slutDen, patient, laegemiddel, klokkeSlet, antal);
        assertNotNull(dagligSkaev);
    }

    @Test
    public void testOpretDagligSkaevOrdination2() {
        //TC2
        LocalDate slutDen = LocalDate.of(2029, 2, 20);
        DagligSkaev dagligSkaev = controller.opretDagligSkaevOrdination(startDen, slutDen, patient, laegemiddel, klokkeSlet, antal);
        assertNotNull(dagligSkaev);
    }

    @Test
    public void testOpretDagligSkaevOrdination3() {
        //TC3
        LocalDate slutDen = LocalDate.of(2020, 3, 20);
        DagligSkaev dagligSkaev = controller.opretDagligSkaevOrdination(startDen, slutDen, patient, laegemiddel, klokkeSlet, antal);
        assertNotNull(dagligSkaev);
    }

    @Test
    public void testOpretDagligSkaevOrdination4() {
        //TC4
        LocalDate slutDen = LocalDate.of(2021, 2, 20);
        DagligSkaev dagligSkaev = controller.opretDagligSkaevOrdination(startDen, slutDen, patient, laegemiddel, klokkeSlet, antal);
        assertNotNull(dagligSkaev);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdination5() {
        //TC5
        LocalDate slutDen = LocalDate.of(2020, 2, 19);
        DagligSkaev dagligSkaev = controller.opretDagligSkaevOrdination(startDen, slutDen, patient, laegemiddel, klokkeSlet, antal);
        assertNotNull(dagligSkaev);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdination6() {
        //TC6
        LocalDate slutDen = LocalDate.of(2020, 2, 19);
        klokkeSlet = new LocalTime[]{LocalTime.of(0, 0)};
        antal = new double[]{-1};
        DagligSkaev dagligSkaev = controller.opretDagligSkaevOrdination(startDen, slutDen, patient, laegemiddel, klokkeSlet, antal);
        assertNotNull(dagligSkaev);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdination7() {
        //TC7
        LocalDate slutDen = LocalDate.of(2020, 2, 19);
        klokkeSlet = null;
        antal = null;
        DagligSkaev dagligSkaev = controller.opretDagligSkaevOrdination(startDen, slutDen, patient, laegemiddel, klokkeSlet, antal);
        assertNotNull(dagligSkaev);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOpretDagligSkaevOrdination8() {
        //TC8
        LocalDate slutDen = LocalDate.of(2020, 2, 19);
        klokkeSlet = new LocalTime[]{};
        antal = new double[]{1};
        DagligSkaev dagligSkaev = controller.opretDagligSkaevOrdination(startDen, slutDen, patient, laegemiddel, klokkeSlet, antal);
        assertNotNull(dagligSkaev);
    }

    // testOrdinationPNAnvendt

    @Test
    public void testOrdinationPNAnvendt1() {
        //TC1
        LocalDate slutDen = LocalDate.of(2020, 2, 27);
        PN pn = new PN(startDen, slutDen, laegemiddel, 123);
        LocalDate dato = startDen;
        controller.ordinationPNAnvendt(pn, dato);
        assertEquals(1, pn.getAntalGangeGivet());

        //TC2
        dato = slutDen;
        controller.ordinationPNAnvendt(pn, dato);
        assertEquals(2, pn.getAntalGangeGivet());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrdinationPNAnvendt3() {
        //TC3
        LocalDate slutDen = LocalDate.of(2020, 2, 27);
        PN pn = new PN(startDen, slutDen, laegemiddel, 123);
        LocalDate dato = startDen.minusDays(1);
        controller.ordinationPNAnvendt(pn, dato);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOrdinationPNAnvendt4() {
        //TC4
        LocalDate slutDen = LocalDate.of(2020, 2, 27);
        PN pn = new PN(startDen, slutDen, laegemiddel, 123);
        LocalDate dato = slutDen.plusDays(1);
        controller.ordinationPNAnvendt(pn, dato);
    }

    // antalOrdinationerPrVægtPrLægemiddel

    @Test
    public void testAntalOrdinationerPrVægtPrLægemiddel() {
        //TC1
        double vægtStart = 0.0;
        double vægtSlut = 0.0;
        controller.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, laegemiddel, 3);
        int antal = controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut, laegemiddel);
        assertEquals(0, antal);

        //TC2
        vægtSlut = 100.0;
        antal = controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut, laegemiddel);
        assertEquals(1, antal);

        //TC3
        vægtStart = 63.4;
        vægtSlut = 63.4;
        antal = controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut, laegemiddel);
        assertEquals(1, antal);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAntalOrdinationerPrVægtPrLægemiddel4() {
        //TC4
        double vægtStart = 50.0;
        double vægtSlut = 49.0;
        controller.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, laegemiddel, 3);
        controller.antalOrdinationerPrVægtPrLægemiddel(vægtStart, vægtSlut, laegemiddel);
    }
}
