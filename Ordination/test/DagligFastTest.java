package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;

import ordination.DagligFast;

public class DagligFastTest {

	private Laegemiddel laegemiddel;
	private LocalDate startDen;
	private LocalDate slutDen;
	private double morgenAntal;
	private double middagAntal;
	private double aftenAntal;
	private double natAntal;

	private DagligFast dagligfast;

	@Before
	public void setUp() {

		startDen = LocalDate.of(2020, 2, 20);
		slutDen = LocalDate.of(2020, 2, 20);
		morgenAntal = 0;
		middagAntal = 0;
		aftenAntal = 0;
		natAntal = 0;
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		dagligfast = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);

	}

	@Test
	// TC1
	public void testDagligFast1() {
		LocalDate slutDen = LocalDate.of(2020, 2, 20);
		morgenAntal = 1;
		middagAntal = 1;
		aftenAntal = 1;
		natAntal = 1;
		dagligfast = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
		assertNotNull(dagligfast);
	}

	@Test
	// TC2
	public void testDagligFast2() {
		LocalDate slutDen = LocalDate.of(2020, 2, 29);
		morgenAntal = 1;
		middagAntal = 1;
		aftenAntal = 1;
		natAntal = 1;
		dagligfast = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
		assertNotNull(dagligfast);
	}

	@Test
	// TC3
	public void testDagligFast3() {
		LocalDate slutDen = LocalDate.of(2020, 3, 20);
		morgenAntal = 1;
		middagAntal = 1;
		aftenAntal = 1;
		natAntal = 1;
		dagligfast = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
		assertNotNull(dagligfast);
	}

	@Test
	// TC4
	public void testDagligFast4() {
		LocalDate slutDen = LocalDate.of(2021, 3, 20);
		morgenAntal = 1;
		middagAntal = 1;
		aftenAntal = 1;
		natAntal = 1;
		dagligfast = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
		assertNotNull(dagligfast);
	}

	@Test
	// TC1
	public void testSamletDosis1() {
		LocalDate slutDen = LocalDate.of(2020, 2, 20);
		morgenAntal = 1;
		middagAntal = 1;
		aftenAntal = 1;
		natAntal = 1;
		dagligfast = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
		assertNotNull(dagligfast);

		int antalDage = dagligfast.antalDage();
		assertEquals(antalDage, 1);

	}

	@Test
	// TC2
	public void testSamletDosis2() {
		LocalDate slutDen = LocalDate.of(2020, 3, 20);
		morgenAntal = 1;
		middagAntal = 1;
		aftenAntal = 1;
		natAntal = 1;
		dagligfast = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
		int antalDage = dagligfast.antalDage();
		assertEquals(antalDage, 30);

	}

	@Test
	// TC1
	public void testDoegnDosis1() {
		LocalDate slutDen = LocalDate.of(2020, 3, 20);
		morgenAntal = 1;
		middagAntal = 1;
		aftenAntal = 1;
		natAntal = 1;
		dagligfast = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
		double antalDoser = dagligfast.doegnDosis();
		assertEquals(antalDoser, 4, 0.01);
	}

	@Test
	// TC2
	public void testDoegnDosis2() {
		LocalDate slutDen = LocalDate.of(2020, 3, 20);
		morgenAntal = 1;
		middagAntal = 1;
		aftenAntal = 1;
		natAntal = 1;
		dagligfast = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal, natAntal);
		dagligfast.doegnDosis();
		double antalDoser = dagligfast.doegnDosis();
		assertEquals(antalDoser, 4, 0.01);

	}

}
