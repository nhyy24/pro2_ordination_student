package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;

public class DagligSkaevTest {

	private Laegemiddel laegemiddel;
	private LocalDate startDen;
	private LocalDate slutDen;
	private DagligSkaev dagligSkaev;

	@Before
	public void setUp() {
		startDen = LocalDate.of(2020, 2, 20);
		slutDen = LocalDate.of(2020, 2, 20);
		double[] antalEnheder = new double[1];
		LocalTime[] klokkeSlet = new LocalTime[1];
		klokkeSlet[0] = LocalTime.of(12, 00);
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

		dagligSkaev = new DagligSkaev(startDen, slutDen, laegemiddel, klokkeSlet, antalEnheder);

	}

	@Test
	public void testDagligSkaev() {
		// TC1
		LocalDate slutDen = LocalDate.of(2020, 2, 20);
		double[] antalEnheder = new double[1];
		LocalTime[] klokkeSlet = new LocalTime[1];
		klokkeSlet[0] = LocalTime.of(12, 00);
		dagligSkaev = new DagligSkaev(startDen, slutDen, laegemiddel, klokkeSlet, antalEnheder);
		assertNotNull(dagligSkaev);

	}

	@Test
	public void testDagligSkaev1() {
		// TC2
		LocalDate slutDen = LocalDate.of(2020, 2, 29);
		double[] antalEnheder = new double[10];
		LocalTime[] klokkeSlet = new LocalTime[1];
		klokkeSlet[0] = LocalTime.of(00, 00);
		dagligSkaev = new DagligSkaev(startDen, slutDen, laegemiddel, klokkeSlet, antalEnheder);
		assertNotNull(dagligSkaev);
	}

	@Test
	public void testDagligSkaev2() {
		// TC3
		LocalDate slutDen = LocalDate.of(2020, 3, 20);
		double[] antalEnheder = new double[1];
		LocalTime[] klokkeSlet = new LocalTime[1];
		klokkeSlet[0] = LocalTime.of(00, 00);
		dagligSkaev = new DagligSkaev(startDen, slutDen, laegemiddel, klokkeSlet, antalEnheder);
		assertNotNull(dagligSkaev);
	}

	@Test
	public void testOpretDosis() {
		// TC1
		LocalTime lt = LocalTime.of(12, 00);
		double antal = 1;
		dagligSkaev.opretDosis(lt, antal);
		Dosis d = dagligSkaev.getDoser().get(1);
		assertEquals(1, d.getAntal(), 0.01);
		assertEquals(d.getTid(), LocalTime.of(12, 00));
	}

	@Test
	public void testOpretDosis1() {
		// TC2
		LocalTime lt = LocalTime.of(00, 00);
		double antal = 5;
		dagligSkaev.opretDosis(lt, antal);
		Dosis d = dagligSkaev.getDoser().get(1);
		assertEquals(5, d.getAntal(), 0.01);
		assertEquals(d.getTid(), LocalTime.of(00, 00));
	}

	@Test
	public void testOpretDosis2() {
		// TC3
		LocalTime lt = LocalTime.of(23, 59);
		double antal = 0.5;
		dagligSkaev.opretDosis(lt, antal);
		Dosis d = dagligSkaev.getDoser().get(1);
		assertEquals(0.5, d.getAntal(), 0.01);
		assertEquals(d.getTid(), LocalTime.of(23, 59));
	}

	@Test
	public void testDoegnDosis() {
		// TC1
		LocalDate slutDen = LocalDate.of(2020, 3, 20);
		double[] antalEnheder = new double[1];
		LocalTime[] klokkeSlet = new LocalTime[1];
		klokkeSlet[0] = LocalTime.of(00, 00);
		dagligSkaev = new DagligSkaev(startDen, slutDen, laegemiddel, klokkeSlet, antalEnheder);
		dagligSkaev.opretDosis(LocalTime.of(12, 00), 1);
		double antalDoser = dagligSkaev.doegnDosis();
		assertEquals(antalDoser, 1, 0.01);

	}

	@Test
	public void testDoegnDosis1() {
		// TC2
		LocalDate slutDen = LocalDate.of(2020, 3, 20);
		double[] antalEnheder = new double[1];
		LocalTime[] klokkeSlet = new LocalTime[1];
		klokkeSlet[0] = LocalTime.of(00, 00);
		dagligSkaev = new DagligSkaev(startDen, slutDen, laegemiddel, klokkeSlet, antalEnheder);
		double antalDoser = dagligSkaev.doegnDosis();
		assertEquals(antalDoser, 0, 0.01);
	}

	@Test
	public void testSamletDosis() {
		// TC1
		LocalDate slutDen = LocalDate.of(2020, 2, 20);
		double[] antalEnheder = new double[1];
		LocalTime[] klokkeSlet = new LocalTime[1];
		klokkeSlet[0] = LocalTime.of(00, 00);
		dagligSkaev = new DagligSkaev(startDen, slutDen, laegemiddel, klokkeSlet, antalEnheder);

		int antalDage = dagligSkaev.antalDage();
		assertEquals(antalDage, 1);
//		assertEquals(, actual);
	}

	@Test
	public void testSamletDosis1() {
		// TC2
		LocalDate slutDen = LocalDate.of(2020, 3, 20);
		double[] antalEnheder = new double[1];
		LocalTime[] klokkeSlet = new LocalTime[1];
		klokkeSlet[0] = LocalTime.of(00, 00);
		dagligSkaev = new DagligSkaev(startDen, slutDen, laegemiddel, klokkeSlet, antalEnheder);
		int antalDage = dagligSkaev.antalDage();
		assertEquals(antalDage, 30);
	}

}
