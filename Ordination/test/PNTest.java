package test;

import ordination.Laegemiddel;
import ordination.PN;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class PNTest {

    private LocalDate startDen;
    private LocalDate slutDen;
    private Laegemiddel laegemiddel;
    private double antal;

    private PN pn;

    @Before
    public void setUp() {
        startDen = LocalDate.of(2020, 2, 20);
        slutDen = LocalDate.of(2020, 2, 27);
        laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        antal = 1.5;
        pn = new PN(startDen, slutDen, laegemiddel, antal);
    }

    @Test
    public void testPN() {
        //TC1
        assertNotNull(pn);
        assertEquals(startDen, pn.getStartDen());
        assertEquals(slutDen, pn.getSlutDen());
        assertEquals(laegemiddel, pn.getLaegemiddel());
        assertEquals(antal, pn.getAntalEnheder(), 0.0001);
    }

    @Test
    public void testGivDosis() {
        //TC1
        LocalDate givesDen = LocalDate.of(2020, 2, 19);
        boolean dosisGivet = pn.givDosis(givesDen);
        assertFalse(dosisGivet);

        //TC2
        givesDen = LocalDate.of(2020, 2, 20);
        dosisGivet = pn.givDosis(givesDen);
        assertTrue(dosisGivet);

        //TC3
        givesDen = LocalDate.of(2020, 2, 27);
        dosisGivet = pn.givDosis(givesDen);
        assertTrue(dosisGivet);

        //TC4
        givesDen = LocalDate.of(2020, 2, 28);
        dosisGivet = pn.givDosis(givesDen);
        assertFalse(dosisGivet);
    }

    @Test
    public void testDoegnDosis() {
        //TC1
        pn.givDosis(startDen);
        pn.givDosis(slutDen);
        assertEquals(0.375, pn.doegnDosis(), 0.0001);
    }

    @Test
    public void testSamletDosis() {
        //TC1
        pn.givDosis(startDen);
        pn.givDosis(slutDen);
        assertEquals(3, pn.samletDosis(), 0.0001);
    }
}
